<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Post extends Model
{
    
	use softDeletes;


	protected $guarded = [];

	public function getFeaturedAttribute($featured)
	{
		return asset($featured);
	}

	protected $dates = ['deleted_at'];

    public function category()
    {
    	return $this->belongsTo('App\category');
    }


    public function tags() {
        return $this->belongsToMany('App\Tag');
    }

}
