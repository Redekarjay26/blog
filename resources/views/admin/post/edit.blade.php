@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Edit Post</div>
        <div class="card-body">
		@include('admin.includes.errors')
        	<form action="{{ route('post.update',['id'=>$post->id]) }}" method="post" enctype="multipart/form-data">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="title">Title:</label>
			    <input type="text" class="form-control" name="title" value="{{$post->title}}">
			  </div>
			  <div class="form-group">
			    <label for="featured">Featured:</label>
			    <input type="file" class="form-control" name="featured">
			  </div>
				<div class="form-group">
					<lable for="category">Select category</lable>
					<select name="category_id" class="form-control">
						@foreach($categories as $category)
							
							<option value="{{$category->id}}"

								@if($post->category->id == $category->id)

								selected

								@endif 

								>{{$category->name}}</option>

						@endforeach
					</select>
				</div>


				<div class="form-group">
					<lable for="">Select Tags</lable></br></br>

						@foreach($tags as $tag)		

						<div class="checkbox">
							<label><input type="checkbox" name="tags[]" value="{{ $tag->id }}"
							@foreach($post->tags as $t)
								@if($tag->id == $t->id)
									checked
								@endif
							@endforeach

								>{{ $tag->tag }}</label>
						</div>

						@endforeach					
				</div>

			  <div class="form-group">
			  	<lable for="content">Content</lable>
			  	<textarea name="content" cols="5" rows="3" class="form-control">{{$post->content}}</textarea>
			  </div>
			  <button type="submit" class="btn btn-success float-right">Update Post</button>
			</form>
						
		</div>
    </div>    


@endsection