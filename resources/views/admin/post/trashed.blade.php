@extends('layouts.app')



@section('content')

<form action="{{route('post.massdelete')}}" method="post">
  {{ csrf_field()}}
	<table class="table table-hover">
    <thead>
      <tr>

        <th>Image</th>
        <th>Title</th>
        <th>Edit</th>
        <th>Restore</th>
        <th>Delete</th>
        <th><button type="submit" class="btn btn-success btn-sm">MassDelete</button></th>
      </tr>
    </thead>
    <tbody>

    	@foreach($posts as $post)
      <tr>

        <td><img src="{{$post->featured}}" alt="{{$post->title}}" width="50px" height="50px"></td>
		<td>{{$post->title}}</td>
       <td> <a href="{{route('post.edit', ['id'=>$post->id])}}" class="btn btn-info">Edit</a></td>
        <td> <a href="{{route('post.restore', ['id'=>$post->id])}}" class="btn btn-success">Restore</a></td>
        <td> <a href="{{route('post.kill', ['id'=>$post->id])}}" class="btn btn-danger">Delete</a></td>
        <td><input type="checkbox" name="delid[]" value="{{$post->id}}"></td>
      </tr>

      	@endforeach
    </tbody>
  </table>

</form>
@endsection