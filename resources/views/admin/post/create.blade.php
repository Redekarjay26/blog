@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Create a new Post</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('post.store') }}" method="post" enctype="multipart/form-data">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="title">Title:</label>
			    <input type="text" class="form-control" name="title">
			  </div>
			  <div class="form-group">
			    <label for="featured">Featured:</label>
			    <input type="file" class="form-control" name="featured">
			  </div>
				<div class="form-group">
					<lable for="category">Select category</lable>
					<select name="category_id" class="form-control">
						@foreach($categories as $category)
							
							<option value="{{$category->id}}">{{$category->name}}</option>

						@endforeach
					</select>
				</div>

				<div class="form-group">
					<lable for="">Select Tags</lable></br></br>

						@foreach($tags as $tag)		

						<div class="checkbox">
							  <label><input type="checkbox" name="tags[]" value="{{ $tag->id }}">{{ $tag->tag }}</label>
						</div>

							@endforeach					
				</div>

			  <div class="form-group">
			  	<lable for="content">Content</lable>
			  	<textarea name="content" id="content" cols="5" rows="3" class="form-control"></textarea>
			  </div>
			  <button type="submit" class="btn btn-success float-right">Create Post</button>
			</form>
						
		</div>
    </div>    


@endsection

@section('styles')

<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet">

@stop


@section('scripts')

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
        $('#content').summernote();
    });
</script>

@stop




