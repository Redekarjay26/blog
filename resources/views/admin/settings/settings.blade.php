@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Edit Bolg setting</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('settings.update') }}" method="post">
        		{{ csrf_field()}}
			  
				<div class="form-group">
			    <label for="site_name">Site Name:</label>
			    <input type="text" class="form-control" name="site_name" value="{{$settings->site_name}}">
			  </div>

			  <div class="form-group">
			    <label for="address">Address:</label>
			    <input type="text" class="form-control" name="address" value="{{$settings->address}}">
			  </div>

			<div class="form-group">
			    <label for="contact_number">Contact Number:</label>
			    <input type="text" class="form-control" name="contact_number" value="{{$settings->contact_number}}">
			</div>

			<div class="form-group">
			    <label for="contact_email">Contact Email:</label>
			    <input type="text" class="form-control" name="contact_email" value="{{$settings->contact_email}}">
			</div>

			  <button type="submit" class="btn btn-success float-right">Update site Setting</button>
			</form>
						
		</div>
    </div>    


@endsection