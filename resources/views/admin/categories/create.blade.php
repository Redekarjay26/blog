@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Create a new caterory</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('category.store') }}" method="post">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="name">Name:</label>
			    <input type="text" class="form-control" name="name">
			  </div>
			  <button type="submit" class="btn btn-success float-right">Create Category</button>
			</form>
						
		</div>
    </div>    


@endsection