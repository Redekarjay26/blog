@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Edit Category : {{$category->name}}</div>
        <div class="card-body">
		@include('admin.includes.errors')
        	<form action="{{ route('category.update', ['id' =>$category->id]) }}" method="post">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="name">Name:</label>
			    <input type="text" class="form-control" name="name" value="{{$category->name}}">
			  </div>
			  <button type="submit" class="btn btn-success float-right">Update Category</button>
			</form>
						
		</div>
    </div>    


@endsection