@extends('layouts.app')

@section('content')

	<div class="card">
        <div class="card-header">Edit your profile</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('user.profile.update') }}" method="post" enctype="multipart/form-data">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="name">User name:</label>
			    <input type="text" class="form-control" value="{{$user->name}}" name="name">
			  </div>

			  <div class="form-group">
			    <label for="email">Email:</label>
			    <input type="email" class="form-control" value="{{$user->email}}" name="email">
			  </div>

			  <div class="form-group">
			    <label for="password">Password:</label>
			    <input type="password" class="form-control" name="password">
			  </div>

			  <div class="form-group">
			    <label for="avatar">Upload New Avatar</label>
			    <input type="file" class="form-control" name="avatar">
			  </div>

			  <div class="form-group">
			    <label for="facebook">Facebook:</label>
			    <input type="text" class="form-control" value="{{$user->profile->facebook}}" name="facebook">
			  </div>

			  <div class="form-group">
			    <label for="toutube">Youtube:</label>
			    <input type="text" class="form-control" value="{{$user->profile->youtube}}" name="youtube">
			  </div>

			  <div class="form-group">
			  	<lable for="about">About</lable>

			  	<textarea name="about" id="about" cols="6" rows="6" class="form-control">{{$user->profile->about}}</textarea>

			  </div>

			  <button type="submit" class="btn btn-success float-right">Update Profile</button>
			</form>
						
		</div>
    </div>    


@endsection