@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Create a new User</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('user.store') }}" method="post">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="name">User name:</label>
			    <input type="text" class="form-control" name="name">
			  </div>
			  <div class="form-group">
			    <label for="enail">Email:</label>
			    <input type="email" class="form-control" name="email">
			  </div>
			  <button type="submit" class="btn btn-success float-right">Create User</button>
			</form>
						
		</div>
    </div>    


@endsection