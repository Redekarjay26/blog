@extends('layouts.app')



@section('content')

	<table class="table table-hover">
    <thead>
      <tr>
        <th>Tag Name</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>

    	@foreach($tags as $tag)
      <tr>

        <td>{{$tag->tag}}</td>

       <td> <a href="{{route('tag.edit', ['id'=>$tag->id])}}" class="btn btn-info">Edit</a></td>
        <td> <a href="{{route('tag.delete', ['id'=>$tag->id])}}" class="btn btn-danger">Delete</a></td>
      </tr>
      	@endforeach
    </tbody>
  </table>


@endsection