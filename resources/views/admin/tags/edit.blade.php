@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Update Tag</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('tag.update',['id'=> $tag->id]) }}" method="post">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="tag">Tag:</label>
			    <input type="text" class="form-control" name="tag" value="{{$tag->tag}}">
			  </div>
			  <button type="submit" class="btn btn-success float-right">Update Tag</button>
			</form>
						
		</div>
    </div>    


@endsection