@extends('layouts.app')



@section('content')

	<div class="card">
        <div class="card-header">Create a new Tag</div>
        <div class="card-body">
		@if ($errors->any())
		    <div class="alert alert-danger">
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif
        	<form action="{{ route('tag.store') }}" method="post">
        		{{ csrf_field()}}
			  <div class="form-group">
			    <label for="tag">Tag:</label>
			    <input type="text" class="form-control" name="tag">
			  </div>
			  <button type="submit" class="btn btn-success float-right">Create Tag</button>
			</form>
						
		</div>
    </div>    


@endsection