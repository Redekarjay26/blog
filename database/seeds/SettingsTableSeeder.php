<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
            'site_name' => "",
            'address' => '',
            'contact_number' => '',
            'contact_email' => ''
        ]);
    }
}
